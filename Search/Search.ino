#include <Wire.h>
#include <Zumo32U4.h>
#include "protothreads.h"

// This might need to be tuned for different lighting conditions,
// surfaces, etc.
#define QTR_THRESHOLD     700  // microseconds

// These might need to be tuned for different motor types.
#define REVERSE_SPEED     200  // 0 is stopped, 400 is full speed
#define TURN_SPEED        300
#define FORWARD_SPEED     400
#define REVERSE_DURATION  200  // ms
#define TURN_DURATION     300  // ms
#define RIGHT             1
#define LEFT              0

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA;
Zumo32U4Buzzer buzzer;
Zumo32U4Motors motors;
Zumo32U4LineSensors lineSensors;
Zumo32U4ProximitySensors proxSensors;

#define NUM_SENSORS 3
unsigned int lineSensorValues[NUM_SENSORS];

// Go straight for multiple loops
int randomTurnCount = 0;

// Line sensor switch flag
int flag = 0;

int walkPause = 0;

// Declare protothreads
static struct pt ptWalk, ptRing, ptSearch;

// Prox threshold
const uint8_t sensorThreshold = 4;

// The maximum speed to drive the motors while turning. 400 is
// full speed.
const uint16_t turnSpeedMax = 400;

// The minimum speed to drive the motors while turning.  400 is
// full speed.
const uint16_t turnSpeedMin = 200;

// The amount to decrease the motor speed by during each cycle
// when an object is seen.
const uint16_t deceleration = 10;

// The amount to increase the speed by during each cycle when an
// object is not seen.
const uint16_t acceleration = 10;

// If the robot is turning, this is the speed it will use.
uint16_t turnSpeed = turnSpeedMax;

// The time, in milliseconds, when an object was last seen.
uint16_t lastTimeObjectSeen = 0;

bool lastSensed = RIGHT;

void waitForButtonAndCountDown()
{
  ledYellow(1);
  lcd.clear();
  lcd.print(F("Press A"));

  buttonA.waitForButton();
  
  ledYellow(0);
  lcd.clear();

  // Countdown.
  for (int i = 0; i < 3; i++)
  {
    lcd.print(i+1);
    delay(1000);
    lcd.clear();
  }
  lcd.print("Moving!");
  delay(1000);
  lcd.clear();
}

void checkLineSensors()
{
  lineSensors.read(lineSensorValues);
  if(lineSensorValues[0] < QTR_THRESHOLD || lineSensorValues[NUM_SENSORS-1] < QTR_THRESHOLD)
  {
    flag = 1;
  }
  else
  {
    flag = 0;
  }
}

uint8_t frontleftValue = 0;
uint8_t frontrightValue = 0;
uint8_t middleleftValue = 0;
uint8_t middlerightValue = 0;

void checkProxSensors()
{
  proxSensors.read();
  frontleftValue = proxSensors.countsFrontWithLeftLeds();
  frontrightValue = proxSensors.countsFrontWithRightLeds();
  middleleftValue = proxSensors.countsLeftWithLeftLeds();
  middlerightValue = proxSensors.countsRightWithRightLeds();
}

// First protothread function to random walk
static int protothreadRandomWalk(struct pt *pt)
{
  static unsigned long lastScan = millis();
  PT_BEGIN(pt);
  while(1) 
  {
    lcd.clear();
    lcd.print("Walk");
    int randomDuration = random(200,300);
    int randomDirection = random(0,30);
    
    PT_WAIT_WHILE(pt, flag == 1);

    if (millis() - lastScan > 50)
    {
      walkPause = 1;
      lastScan = millis();
    }

    PT_WAIT_UNTIL(pt, walkPause == 0);
    
    if (randomTurnCount == 100)
    {
      randomTurnCount = 0;
    }
    
    if (randomDirection <= 3 && randomTurnCount == 0)
    {
      randomTurnCount++;
      motors.setSpeeds(-TURN_SPEED, TURN_SPEED); // turn left
      delay(randomDuration);
    }
    else if (randomDirection >= 27 && randomTurnCount == 0)
    {
      randomTurnCount++;
      motors.setSpeeds(TURN_SPEED, -TURN_SPEED); // turn right
      delay(randomDuration);
    }
    else
    {
      randomTurnCount++;
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED); // go straight
    }
    checkLineSensors();
  }
  PT_END(pt);
}

// Second protothread function to stay in the ring
static int protothreadStayInRing(struct pt *pt)
{
  PT_BEGIN(pt);
  while(1) 
  {
    lcd.clear();
    lcd.print("Ring");
    lineSensors.read(lineSensorValues);
    
    PT_WAIT_WHILE(pt,flag != 1);
    
    if (lineSensorValues[0] < QTR_THRESHOLD)
    {
      // If leftmost sensor detects line, reverse and turn to the
      // right.
      motors.setSpeeds(-REVERSE_SPEED, -REVERSE_SPEED);
      delay(REVERSE_DURATION);
      motors.setSpeeds(TURN_SPEED, -TURN_SPEED);
      delay(TURN_DURATION);
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED);
    }
    else if (lineSensorValues[NUM_SENSORS - 1] < QTR_THRESHOLD)
    {
      // If rightmost sensor detects line, reverse and turn to the
      // left.
      motors.setSpeeds(-REVERSE_SPEED, -REVERSE_SPEED);
      delay(REVERSE_DURATION);
      motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
      delay(TURN_DURATION);
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED);
    }
    checkLineSensors();
  }
  PT_END(pt);
}

// Third protothread to scan the ring for opponents
static int protothreadSearch(struct pt *pt)
{
  int objectSeen = 0;
  PT_BEGIN(pt);
  while(1)
  {
    lcd.clear();
    lcd.print("Scan");
    checkProxSensors();
    
    PT_WAIT_WHILE(pt, flag == 1);

    if (frontleftValue >= sensorThreshold || frontrightValue >= sensorThreshold || middleleftValue >= sensorThreshold || middlerightValue >= sensorThreshold)
    {
      objectSeen = 1;
    }

    if (objectSeen == 1)
    {
      turnSpeed -= deceleration;
    }
    else
    {
      turnSpeed += acceleration;
    }

    turnSpeed = constrain(turnSpeed, turnSpeedMin, turnSpeedMax);

    if (objectSeen == 1)
    {
      lastTimeObjectSeen = millis();

      if ((frontleftValue + middleleftValue) < (frontrightValue + middlerightValue))
      {
        motors.setSpeeds(turnSpeed, -turnSpeed); // turn right
        lastSensed = RIGHT;
      }
      else if ((frontleftValue + middleleftValue) > (frontrightValue + middlerightValue))
      {
        motors.setSpeeds(-turnSpeed, turnSpeed); // turn left
        lastSensed = LEFT;
      }
      else 
      { // put this in a separate protothread (ptAttack) later?
        while (1)
        {
          if (frontleftValue >= sensorThreshold || frontrightValue >= sensorThreshold || middleleftValue >= sensorThreshold || middlerightValue >= sensorThreshold)
            motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED);
          else
            break;
          checkProxSensors();
          checkLineSensors();
          PT_WAIT_WHILE(pt, flag == 1);
        }
      }
    }
    else if (millis() - lastTimeObjectSeen > 100)
    {
      walkPause = 0;
      PT_WAIT_UNTIL(pt, walkPause == 1);
    }
    else
    {
      if (lastSensed == RIGHT)
      {
        motors.setSpeeds(turnSpeed, -turnSpeed);
      }
      else
      {
        motors.setSpeeds(-turnSpeed, turnSpeed);
      }
    }
  }
  PT_END(pt);
}

// init all protothreads
void setup() 
{
  PT_INIT(&ptWalk);
  PT_INIT(&ptRing);
  PT_INIT(&ptSearch);
  lineSensors.initThreeSensors();
  proxSensors.initThreeSensors();
  waitForButtonAndCountDown();
}

// In the loop we just need to call the protothreads one by one
void loop() 
{
  protothreadRandomWalk(&ptWalk);
  protothreadStayInRing(&ptRing);
  protothreadSearch(&ptSearch);
}
