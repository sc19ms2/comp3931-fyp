// WaitingMode

#include <Wire.h>
#include <Zumo32U4.h>

Zumo32U4Buzzer buzzer;
Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA;

void setup()
{
  lcd.print(F("Press A"));
  buttonA.waitForButton();
  lcd.clear();

  for (int i = 0; i <= 5; i++) 
  {
    if (i != 5)
    {
      delay(995);
      buzzer.playNote(NOTE_A(4), 1, 15);
    }
    else
    {
      buzzer.playNote(NOTE_A(4), 1, 15);
      break; 
    }
  }
}

void loop()
{
  lcd.print("i waited!");
  delay(1000);
  lcd.clear();
}
