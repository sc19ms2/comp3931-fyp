#include <avr/pgmspace.h>
#include <Wire.h>
#include <Zumo32U4.h>
#include "protothreads.h"

#define REVERSE_SPEED     200  // 0 is stopped, 400 is full speed
#define TURN_SPEED        300
#define FORWARD_SPEED     200
#define CHARGE_SPEED      400
#define REVERSE_DURATION  200  
#define TURN_DURATION     300  
#define RIGHT             1
#define LEFT              0

Zumo32U4LCD lcd;
Zumo32U4ButtonA buttonA;
Zumo32U4Buzzer buzzer;
Zumo32U4Motors motors;
Zumo32U4LineSensors lineSensors;
Zumo32U4ProximitySensors proxSensors;

// Declare protothreads
static struct pt ptWalk, ptRing, ptSearch, ptDefend;

// Line Sensor Stuff
#define QTR_THRESHOLD     700  
#define NUM_SENSORS       3
unsigned int lineSensorValues[NUM_SENSORS];

// Accelerometer Settings
#define RA_SIZE 3  // number of readings to include in running average of accelerometer readings
#define XY_ACCELERATION_THRESHOLD 2400  // for detection of contact (~16000 = magnitude of acceleration due to gravity)
// Timing
unsigned long loop_start_time = 0;
unsigned long last_turn_time = 0;
unsigned long contact_made_time = 0;
#define MIN_DELAY_AFTER_TURN          400  // min delay before detecting contact event
#define MIN_DELAY_BETWEEN_CONTACTS    1000  // min delay between detecting new contact event
static long threshold_squared = 0;

// RunningAverage class
// based on RunningAverage library for Arduino
// source:  http://playground.arduino.cc/Main/RunningAverage
template <typename T>
class RunningAverage
{
  public:
    RunningAverage(void);
    RunningAverage(int);
    ~RunningAverage();
    void clear();
    void addValue(T);
    T getAverage() const;
    void fillValue(T, int);
  protected:
    int _size;
    int _cnt;
    int _idx;
    T _sum;
    T * _ar;
    static T zero;
};

// Accelerometer Class -- extends the Zumo32U4IMU class to support reading and
// averaging the x-y acceleration vectors from the accelerometer
class Accelerometer : public Zumo32U4IMU
{
  typedef struct acc_data_xy
  {
    unsigned long timestamp;
    int x;
    int y;
    float dir;
  } acc_data_xy;

  public:
    Accelerometer() : ra_x(RA_SIZE), ra_y(RA_SIZE) {};
    ~Accelerometer() {};
    void getLogHeader(void);
    void readAcceleration(unsigned long timestamp);
    float len_xy() const;
    float dir_xy() const;
    int x_avg(void) const;
    int y_avg(void) const;
    long ss_xy_avg(void) const;
    float dir_xy_avg(void) const;
  private:
    acc_data_xy last;
    RunningAverage<int> ra_x;
    RunningAverage<int> ra_y;
};

Accelerometer acc;

// Flags

int randomTurnCount = 0; // Go straight for multiple loops
int borderDetect = 0; // Flag to detect white border
int walkPause = 0; // Flag to yield walk thread to perform scan
int objectSeen = 0; // Flag to detect opponent
int contactMade = 0; // Flag to detect collision
int attackFlag = 0;

// Prox Sensing Stuff
const uint8_t sensorThreshold = 3; // Prox threshold
const uint16_t turnSpeedMax = 400;
const uint16_t turnSpeedMin = 200;
const uint16_t deceleration = 10; // The amount to decrease the motor speed by during each cycle when an object is seen.
const uint16_t acceleration = 10;
uint16_t turnSpeed = turnSpeedMax; // If the robot is turning, this is the speed it will use.
uint16_t lastTimeObjectSeen = 0;
bool lastSensed = RIGHT;
// Prox sensor values
uint8_t frontleftValue = 0;
uint8_t frontrightValue = 0;
uint8_t middleleftValue = 0;
uint8_t middlerightValue = 0;

// Wait 5 seconds when the match starts
void waitForButtonAndCountDown()
{
  ledYellow(1);
  lcd.clear();
  lcd.print(F("Press A"));

  buttonA.waitForButton();
  
  ledYellow(0);
  lcd.clear();

  // Countdown.
  for (int i = 0; i < 5; i++)
  {
    lcd.print(i+1);
    delay(1000);
    lcd.clear();
  }
  lcd.print("Moving!");
  delay(100);
  lcd.clear();
}

int checkLineSensors()
{
  lineSensors.read(lineSensorValues);
  if(lineSensorValues[0] < QTR_THRESHOLD || lineSensorValues[NUM_SENSORS-1] < QTR_THRESHOLD)
    borderDetect = 1;
  else
    borderDetect = 0;
  return borderDetect;
}

void checkProxSensors()
{
  proxSensors.read();
  frontleftValue = proxSensors.countsFrontWithLeftLeds();
  frontrightValue = proxSensors.countsFrontWithRightLeds();
  middleleftValue = proxSensors.countsLeftWithLeftLeds();
  middlerightValue = proxSensors.countsRightWithRightLeds();

  if (frontleftValue >= sensorThreshold || frontrightValue >= sensorThreshold || middleleftValue >= sensorThreshold || middlerightValue >= sensorThreshold)
    objectSeen = 1;
  else
    objectSeen = 0;
  return objectSeen;
}

// check for collisions
bool collisionDetect()
{
  threshold_squared = (long) XY_ACCELERATION_THRESHOLD * (long) XY_ACCELERATION_THRESHOLD;
  return (acc.ss_xy_avg() >  threshold_squared) && (loop_start_time - last_turn_time > MIN_DELAY_AFTER_TURN);
}

// First protothread function to random walk
static int protothreadRandomWalk(struct pt *pt)
{
  //static unsigned long lastScan = millis();
  PT_BEGIN(pt);
  while(1) 
  {
    lcd.clear();
    lcd.print("Walk ");
    int randomDuration = random(200,300);
    int randomDirection = random(0,30);
    
    checkLineSensors();
    PT_WAIT_WHILE(pt, borderDetect == 1);

    //checkProxSensors();
    //PT_WAIT_WHILE(pt, objectSeen == 1);

    collisionDetect();
    PT_WAIT_WHILE(pt,contactMade == 1);

   // if (millis() - lastScan > 50)
  //  {
  //    walkPause = 1;
  //    lastScan = millis();
  //  }

    //PT_WAIT_UNTIL(pt, walkPause == 0);
    
    if (randomTurnCount == 100)
    {
      randomTurnCount = 0;
    }
    
    if (randomDirection <= 3 && randomTurnCount == 0)
    {
      randomTurnCount++;
      motors.setSpeeds(-TURN_SPEED, TURN_SPEED); // turn left
      delay(randomDuration);
      last_turn_time = millis();
    }
    else if (randomDirection >= 27 && randomTurnCount == 0)
    {
      randomTurnCount++;
      motors.setSpeeds(TURN_SPEED, -TURN_SPEED); // turn right
      delay(randomDuration);
      last_turn_time = millis();
    }
    else
    {
      randomTurnCount++;
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED); // go straight
    }
    checkLineSensors();
  }
  PT_END(pt);
}

// Second protothread function to stay in the ring
static int protothreadStayInRing(struct pt *pt)
{
  PT_BEGIN(pt);
  while(1) 
  {
    lcd.clear();
    lcd.print("Ring");
    checkLineSensors();
    
    PT_WAIT_WHILE(pt, borderDetect != 1);
    
    if (lineSensorValues[0] < QTR_THRESHOLD)
    {
      // If leftmost sensor detects line, reverse and turn to the right.
      motors.setSpeeds(-REVERSE_SPEED, -REVERSE_SPEED);
      delay(REVERSE_DURATION);
      motors.setSpeeds(TURN_SPEED, -TURN_SPEED);
      delay(TURN_DURATION);
      last_turn_time = millis();
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED);
    }
    else if (lineSensorValues[NUM_SENSORS - 1] < QTR_THRESHOLD)
    {
      // If rightmost sensor detects line, reverse and turn to the left.
      motors.setSpeeds(-REVERSE_SPEED, -REVERSE_SPEED);
      delay(REVERSE_DURATION);
      motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
      delay(TURN_DURATION);
      last_turn_time = millis();
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED);
    }
    checkLineSensors();
  }
  PT_END(pt);
}

// Third protothread to scan the ring for opponents
static int protothreadSearch(struct pt *pt)
{
  PT_BEGIN(pt);
  while(1)
  {
    lcd.clear();
    lcd.print("Scan");
    checkProxSensors();
    checkLineSensors();

    PT_WAIT_WHILE(pt, borderDetect == 1); 

    collisionDetect();
    PT_WAIT_WHILE(pt,contactMade == 1);

    if (objectSeen == 1)
    {
      turnSpeed -= deceleration;
    }
    else
    {
      turnSpeed += acceleration;
    }

    turnSpeed = constrain(turnSpeed, turnSpeedMin, turnSpeedMax);

    if (objectSeen == 1)
    {
      lastTimeObjectSeen = millis();

      if ((frontleftValue + middleleftValue) < (frontrightValue + middlerightValue))
      {
        motors.setSpeeds(turnSpeed, -turnSpeed); // turn right
        last_turn_time = millis();
        lastSensed = RIGHT;
      }
      else if ((frontleftValue + middleleftValue) > (frontrightValue + middlerightValue))
      {
        motors.setSpeeds(-turnSpeed, turnSpeed); // turn left
        last_turn_time = millis();
        lastSensed = LEFT;
      }
      else 
      { // put this in a separate protothread (ptAttack) later?
        while (1)
        {
          if (objectSeen == 1)
            motors.setSpeeds(CHARGE_SPEED, CHARGE_SPEED);
          else
            break;
          checkProxSensors();
          checkLineSensors();
          PT_WAIT_WHILE(pt, borderDetect == 1);
        }
      }
    }
    else if (millis() - lastTimeObjectSeen > 100)
    {
      PT_WAIT_UNTIL(pt, objectSeen == 1);
    }
    else
    {
      if (lastSensed == RIGHT)
      {
        motors.setSpeeds(turnSpeed, -turnSpeed); // turn right
        last_turn_time = millis();
      }
      else
      {
        motors.setSpeeds(-turnSpeed, turnSpeed); // turn left
        last_turn_time = millis();
      }
    }
  }
  PT_END(pt);
}

// Fourth protothread to defend, i.e. collision detection & response
static int protothreadDefend(struct pt *pt)
{
  PT_BEGIN(pt);
  while(1)
  {
    lcd.clear();
    lcd.print("Defend");
    checkLineSensors();
    //checkProxSensors();

    //PT_WAIT_WHILE(pt,borderDetect == 1);

    loop_start_time = millis();
    acc.readAcceleration(loop_start_time);
    //collisionDetect();

    if (lineSensorValues[0] < QTR_THRESHOLD)
    {
      // If leftmost sensor detects line, reverse and turn to the right.
      motors.setSpeeds(-REVERSE_SPEED, -REVERSE_SPEED);
      delay(REVERSE_DURATION);
      motors.setSpeeds(TURN_SPEED, -TURN_SPEED);
      delay(TURN_DURATION);
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED);
      last_turn_time = millis();
    }
    else if (lineSensorValues[NUM_SENSORS - 1] < QTR_THRESHOLD)
    {
      // If rightmost sensor detects line, reverse and turn to the left.
      motors.setSpeeds(-REVERSE_SPEED, -REVERSE_SPEED);
      delay(REVERSE_DURATION);
      motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
      delay(TURN_DURATION);
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED);
      last_turn_time = millis();
    }
    else   // if hit from front/sides
    {
      if (collisionDetect())
      {
        contactMade = 1;
        contact_made_time = loop_start_time;
        motors.setSpeeds(CHARGE_SPEED, CHARGE_SPEED);
      }
      contactMade = 0;
      motors.setSpeeds(FORWARD_SPEED, FORWARD_SPEED);
    }
    //else if (contactMade == 1 && objectSeen != 1) // if hit from behind
    //{
      //motors.setSpeeds(-TURN_SPEED, TURN_SPEED);
      //delay(TURN_DURATION);
      //last_turn_time = millis();
      //motors.setSpeeds(CHARGE_SPEED, CHARGE_SPEED);
    //}
    // no collision, wait
    //PT_WAIT_WHILE(pt, contactMade != 1);
  }
  PT_END(pt);
}

// init all protothreads and sensors; begin 5 second countdown
void setup() 
{
  //PT_INIT(&ptWalk);
  //PT_INIT(&ptRing);
  //PT_INIT(&ptSearch);
  PT_INIT(&ptDefend);
  Wire.begin();
  lineSensors.initThreeSensors();
  proxSensors.initThreeSensors();
  acc.init();
  acc.enableDefault();
  waitForButtonAndCountDown();
  last_turn_time = millis();
}

// In the loop we just need to call the protothreads one by one
void loop() 
{
  //protothreadRandomWalk(&ptWalk);
  //protothreadStayInRing(&ptRing);
  //protothreadSearch(&ptSearch);
  protothreadDefend(&ptDefend);
}

// class Accelerometer -- member function definitions

void Accelerometer::getLogHeader(void)
{
  Serial.print("millis    x      y     len     dir  | len_avg  dir_avg  |  avg_len");
  Serial.println();
}

void Accelerometer::readAcceleration(unsigned long timestamp)
{
  readAcc();
  if (a.x == last.x && a.y == last.y) return;

  last.timestamp = timestamp;
  last.x = a.x;
  last.y = a.y;

  ra_x.addValue(last.x);
  ra_y.addValue(last.y);
}

float Accelerometer::len_xy() const
{
  return sqrt(last.x*a.x + last.y*a.y);
}

float Accelerometer::dir_xy() const
{
  return atan2(last.x, last.y) * 180.0 / M_PI;
}

int Accelerometer::x_avg(void) const
{
  return ra_x.getAverage();
}

int Accelerometer::y_avg(void) const
{
  return ra_y.getAverage();
}

long Accelerometer::ss_xy_avg(void) const
{
  long x_avg_long = static_cast<long>(x_avg());
  long y_avg_long = static_cast<long>(y_avg());
  return x_avg_long*x_avg_long + y_avg_long*y_avg_long;
}

float Accelerometer::dir_xy_avg(void) const
{
  return atan2(static_cast<float>(x_avg()), static_cast<float>(y_avg())) * 180.0 / M_PI;
}



// RunningAverage class
// based on RunningAverage library for Arduino
// source:  http://playground.arduino.cc/Main/RunningAverage
// author:  Rob.Tillart@gmail.com
// Released to the public domain

template <typename T>
T RunningAverage<T>::zero = static_cast<T>(0);

template <typename T>
RunningAverage<T>::RunningAverage(int n)
{
  _size = n;
  _ar = (T*) malloc(_size * sizeof(T));
  clear();
}

template <typename T>
RunningAverage<T>::~RunningAverage()
{
  free(_ar);
}

// resets all counters
template <typename T>
void RunningAverage<T>::clear()
{
  _cnt = 0;
  _idx = 0;
  _sum = zero;
  for (int i = 0; i< _size; i++) _ar[i] = zero;  // needed to keep addValue simple
}

// adds a new value to the data-set
template <typename T>
void RunningAverage<T>::addValue(T f)
{
  _sum -= _ar[_idx];
  _ar[_idx] = f;
  _sum += _ar[_idx];
  _idx++;
  if (_idx == _size) _idx = 0;  // faster than %
  if (_cnt < _size) _cnt++;
}

// returns the average of the data-set added so far
template <typename T>
T RunningAverage<T>::getAverage() const
{
  if (_cnt == 0) return zero; // NaN ?  math.h
  return _sum / _cnt;
}

// fill the average with a value
// the param number determines how often value is added (weight)
// number should preferably be between 1 and size
template <typename T>
void RunningAverage<T>::fillValue(T value, int number)
{
  clear();
  for (int i = 0; i < number; i++)
  {
    addValue(value);
  }
}
