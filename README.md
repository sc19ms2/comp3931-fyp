# COMP3931: FYP

## Order of development for an autonomous sumo wrestling bot:

1. **WaitingMode.ino:** Implements 5-second wait at the start of the match.
2. **StayInRing.ino:** Enables the Zumo to detect the white border and turn away from it.
3. **FindAndFollow.ino:** Enables the Zumo to scan the ring for an opponent. Once found, the Zumo follows it.
4. **RandomWalk.ino:** Implements pseudo-random movements to allow the Zumo to search the ring.
5. **Search.ino:** Combines the functionalities of StayInRing.ino, FindAndFollow.ino, and RandomWalk.ino using protothreads.
6. **Sumo.ino:** Adding collision detection as protothread to Search.ino.
7. **Sumo2.ino:** Improved version of Sumo.ino. This is the final system implementation.